#!/bin/bash

# Xavier Belanger
# February 17, 2020

# this script is covered by the BSD 2-Clause License

# put this file in $HOME/.kde/Autostart and make it executable
# add the following lines at the end of your .profile
# if [ -f $HOME/.agent-info ]
# then
# 	source $HOME/.agent-info
# fi

# create a temporary script to ask a public key passphrase
ssh_askpass_kdialog ()
{
	(
	/bin/cat << EOF
#!/bin/bash

# Xavier Belanger
# June 6, 2014

exec /usr/bin/kdialog --password "Please enter your passphrase for <b>$1</b>:" --title "SSH Agent"

# EoF
EOF
	) > $OUTFILE
	/bin/chmod 0700 $OUTFILE
}

# if an agent already exist, kill it
if [ -f $HOME/.agent-info ]
then
	agentPid=$(/usr/bin/grep ^SSH_AGENT_PID $HOME/.agent-info | /usr/bin/cut -d ";" -f 1 | /usr/bin/cut -d "=" -f 2)
	/usr/bin/ps -h $agentPid | /usr/bin/grep --quiet "[[:space:]]/usr/bin/ssh-agent$"

	if [ $? -eq 0 ]
	then
		source $HOME/.agent-info
		/usr/bin/ssh-agent -k | /bin/head -2 > $HOME/.agent-kill
		source $HOME/.agent-kill
	fi

	/bin/rm $HOME/.agent-info $HOME/.agent-kill
fi

/usr/bin/kdialog --yesno "Do you want to launch SSH agent?" --title "SSH Agent"

if [ $? -eq 0 ]
then
	# set the variable for the temporary script
	OUTFILE=$HOME/ssh-askpass-KDE.sh
	export SSH_ASKPASS="$OUTFILE"

	# launch a fresh ssh agent 
	/usr/bin/ssh-agent | /bin/head -2 > $HOME/.agent-info
	/bin/chmod 0600 $HOME/.agent-info
	source $HOME/.agent-info

	# load the keys
	# - from a list
	# for pubkey in id_rsa-key-1 id_rsa-key-2
	# - or for all keys
	for pubkey in $(/bin/ls $HOME/.ssh/*.pub)
	do
		key=$(/bin/basename $pubkey .pub)
		ssh_askpass_kdialog $key
		/bin/true | /usr/bin/ssh-add  $HOME/.ssh/$key
		/bin/rm $OUTFILE
	done
fi

# EoF
