# ssh-agent-KDE.sh

This script will allow you to load all your SSH keys
when starting a KDE4 session.

 - Copy the script in $HOME/.kde/Autostart and make
it executable (chmod +x ssh-agent-KDE.sh)

 - In your .profile file add the following lines; this will
load the information from the SSH agent, if available

```
if [ -f $HOME/.agent-info ]
then
      source $HOME/.agent-info
fi
```

 - When using multiple SSH keys, make sure to define a user
friendly name for each pair (for instance 'gitlab'/'gitlab.pub').
The script will use the name of the private key file ('gitlab')
in the popup window when prompting for the passphrase.

 - To check that your keys have been properly added, you can
use the following command in a terminal window:

  `ssh-add -l`


